xquery version "1.0" encoding "Utf-8";

(: _input_schema=ProductOrder :)
(: _output_schema= :)
(: _input_element=salesOrderInsertResponse :)
(: _output_element= :)

declare namespace ns0 = "http://elements.order.customer.cdm.com";
declare namespace xf = "urn:aorta:example_response_tr";



declare function xf:Example_response_tr($request as element(*))
    as element(*) {
        
            let $resultCode := $request//*:salesOrderInsertResponse/*:responseCode
            let $resultMessage := $request//*:salesOrderInsertResponse/*:description
            return

		<AdapterMessage>
		  <ErrorCode>{data($resultCode)}</ErrorCode>
		  <ErrorMessage>{data($resultMessage)}</ErrorMessage>
		  <payload>
		    <Order>
		      <Header>
		        <ErrorCode>{data($resultCode)}</ErrorCode>
		        <ErrorMessage>{data($resultMessage)}</ErrorMessage>
		      </Header>
		    </Order>
		  </payload>
		</AdapterMessage>
};

declare variable $request as element(*) external;


xf:Example_response_tr($request)
