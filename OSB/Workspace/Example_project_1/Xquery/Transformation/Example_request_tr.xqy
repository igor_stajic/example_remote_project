xquery version "1.0" encoding "Utf-8";

(: _input_schema=ProductOrder :)
(: _output_schema= :)
(: _input_element=salesOrderInsertRequest :)
(: _output_element= :)

declare namespace xf = "urn:aorta:example_request_tr";



declare function xf:Example_request_tr($request as element(*))
    as element(*) {
        
            let $transID := $request//*:salesOrderInsertRequest/*:salesOrder/*:salesOrderDetails/*:sourceTransactionId
            return

		<AdapterMessage>
		  <payload>
		    <AddSubscriber>
		      <Originator>{data($request//*:salesOrderInsertRequest/*:salesOrder/*:salesOrderDetails/*:orderSource)}</Originator>
		      <subscriberID>{data($transID)}</subscriberID>
		      <BodyData>
		        <COSID>{data($request//*:salesOrderInsertRequest/*:salesOrder/*:salesOrderDetails/*:buyerGLN)}</COSID>
		        <ICCID>{data($request//*:salesOrderInsertRequest/*:salesOrder/*:salesOrderDetails/*:vendorGLN)}</ICCID>
		      </BodyData>
		    </AddSubscriber>
		    <transactionID>{data($transID)}</transactionID>
		  </payload>
		  <httpauth/>
		  <soapAction>addsubscriber</soapAction>
		</AdapterMessage>
};

declare variable $request as element(*) external;


xf:Example_request_tr($request)
