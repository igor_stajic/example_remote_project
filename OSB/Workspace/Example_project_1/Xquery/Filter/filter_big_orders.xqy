xquery version "1.0" encoding "Cp1252";

(: _is_complex=False :)

declare namespace xf = "urn:aorta:filter8f682940-9a46-4861-b9ca-3b41a6dec2da";

declare function xf:filter_big_orders($request as element(*))
as xs:string {

	    if ( count($request//*:salesOrderInsertRequest/*:salesOrder/*:salesOrderDetails/*:salesOrderLines)> 50)
		then("CONTINUE")
		
		else("DISCARD_NO_AUDIT")
		
};

declare variable $request as element(*) external;
xf:filter_big_orders($request)