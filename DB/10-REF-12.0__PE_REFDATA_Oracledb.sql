insert Into Reference_Data(REFERENCE_DATA_ID,INSTANCE_NAME,KEY1,KEY2,DESCRIPTION,EFFECTIVE_DATE,ACTIVE) values(1000,'NL','example','provisioning','provisioning',sysdate,'T');

insert into Reference_data_value(REFERENCE_DATA_ID, READ_ORDER, VALUE, NAME) values(1000,1,'originator','sif');
insert into Reference_data_value(REFERENCE_DATA_ID, READ_ORDER, VALUE, NAME) values(1000,2,'order_status','new');


commit;