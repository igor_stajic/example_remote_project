insert into lookup_target(NAME, EXECUTION_PLAN, ROLLBACK, ID, PROJECT_NAME) values('Example_EP_1', 'oramds:/soa/apps/Example_project_1/executionPlans/Example_EP_1.xml', 'None', 2, 'Example_project_1');
insert into lookup_target(NAME, EXECUTION_PLAN, ROLLBACK, ID, PROJECT_NAME) values('Example_EP_1', 'oramds:/soa/apps/Example_project_1/executionPlans/Example_EP_1.xml', 'None', 1, 'Example_project_1');
insert into lookup_field(NAME, XPATH, ID, PROJECT_NAME) values('getEP1', 'Domain', 2, 'Example_project_1');
insert into lookup_field(NAME, XPATH, ID, PROJECT_NAME) values('getEP1', 'Affiliate', 1, 'Example_project_1');
insert into lookup_rule(ID, NAME, VALUE, TARGET_ID, PROJECT_NAME, FIELD_ID) values(lookup_rule_se1q.NEXTVAL, 'getEP1', 'CRM', 2, 'Example_project_1', 2);
insert into lookup_rule(ID, NAME, VALUE, TARGET_ID, PROJECT_NAME, FIELD_ID) values(lookup_rule_se1q.NEXTVAL, 'getEP1', 'NL', 1, 'Example_project_1', 1);
commit;